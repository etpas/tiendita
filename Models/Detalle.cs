﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tiendita.Models
{
    class Detalle
    {
        public int Id { get; set; }

        public int ProductoId { get; set; }
        public Producto Producto { get; set; }

        public int VentaId { get; set; }
        public Venta Venta { get; set; }

        public decimal Subtotal { get; set; }

    }
}
