﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tiendita.Models
{
    public class Usuario
    {
        public Usuario()
        {
            FechaDeAlta = DateTime.Now;
        }

        public int Id { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }

        public DateTime FechaDeAlta { get; set; }
    }
}
