﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Tiendita.Models;

namespace Tiendita
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }

        static void Menu()
        {
            Console.Clear();
            Console.WriteLine("----------Menú de inicio-------");
            Console.WriteLine(" 1) Iniciar sesión");
            Console.WriteLine(" 2) Crear un nuevo usuario");
            Console.WriteLine(" 0) Salir \n" );
            Console.Write("Seleccione un número:");
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    Login(); 
                break;

                case "2":
                    CrearUsuario();
                    break;

                case "0": return;
            }
            Menu();
        }

        static void MenuGeneral()
        {
            Console.Clear();
            Console.WriteLine("Menú principal");
            Console.WriteLine(" 1) Módulo de producto");
            Console.WriteLine(" 2) Módulo de venta");
            Console.WriteLine(" 3) Módulo Detalle");
            Console.WriteLine(" 0) Salir \n");
            Console.Write("Seleccione un número:");
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    MenuProducto();
                    break;

                case "2":
                    MenuVenta();
                    break;

                case "3":
                    MenuDetalle();
                    break;

                case "0": Menu(); return;
            }
            MenuProducto();

        }


        static void MenuProducto()
        {
            Console.WriteLine("Menú de productos");
            Console.WriteLine(" 1) Buscar producto");
            Console.WriteLine(" 2) Crear producto");
            Console.WriteLine(" 3) Eliminar producto");
            Console.WriteLine(" 4) Actualizar producto");
            Console.WriteLine(" 0) Salir \n");
            Console.Write("Seleccione un número:");
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    BuscarProductos();
                    break;

                case "2":
                    CrearProducto();
                    break;

                case "3":
                    EliminarProducto();
                    break;

                case "4":
                    ActualizarProducto();
                    break;

                case "0": MenuGeneral(); return;
            }
            MenuProducto();

        }

        static void MenuDetalle()
        {
            
            Console.WriteLine("Menú de detalle");
            Console.WriteLine(" 1) Buscar detalle");
            Console.WriteLine(" 2) Crear detalle");
            Console.WriteLine(" 3) Eliminar detalle");
            Console.WriteLine(" 4) Actualizar detalle");
            Console.WriteLine(" 0) Salir \n");
            Console.Write("Seleccione un número:");
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    BuscarDetalle();
                    break;

                case "2":
                    CrearDetalle();
                    break;

                case "3":
                    EliminarDetalle();
                    break;

                case "4":
                    ActualizarDetalle();
                    break;

                case "0": MenuGeneral(); return;
            }
            MenuDetalle();
        }
        static void MenuVenta()
        {
            Console.WriteLine("Menú de ventas");
            Console.WriteLine(" 1) Buscar venta");
            Console.WriteLine(" 2) Crear venta");
            Console.WriteLine(" 3) Eliminar venta");
            Console.WriteLine(" 4) Actualizar venta");
            Console.WriteLine(" 0) Salir \n");
            Console.Write("Seleccione un número:");
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    BuscarProductos();
                    break;

                case "2":
                    CrearVenta();
                    break;

                case "3":
                    EliminarVenta();
                    break;

                case "4":
                    ActualizarVenta();
                    break;

                case "0": MenuGeneral(); return;
            }
            MenuVenta();
        }
        /// <summary>
        /// Método para generar Hash
        /// </summary>
        /// <param name="hashAlgorithm"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string GetHash(HashAlgorithm hashAlgorithm, string input)
        {

            byte[] data = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Valor hexadecimal
            return sBuilder.ToString();
        }

        /// <summary>
        /// Método para verificar el Hash
        /// </summary>
        /// <param name="hashAlgorithm"></param>
        /// <param name="input"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        private static bool VerifyHash(HashAlgorithm hashAlgorithm, string input, string hash)
        {
            var hashOfInput = GetHash(hashAlgorithm, input);

            // Comparacion
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            return comparer.Compare(hashOfInput, hash) == 0;
        }

        /// <summary>
        /// Crear Usuario
        /// </summary>
        /// 
        public static void CrearUsuario()
        {
            try
            {
                Console.Write("Correo: ");
                string correo = Console.ReadLine();
                Console.Write("Contraseña: ");
                string password = Console.ReadLine();
                Console.WriteLine("Guardando información...");
                string passwordEncrypted = GetHash( new MD5CryptoServiceProvider(), password);
                var usuario = new Usuario
                {
                    Correo = correo,
                    Password = passwordEncrypted,
                };
               
                using (TienditaContext context = new TienditaContext())
                {
                    bool correoExiste = context.Usuarios.Any(x => x.Correo.Equals(correo));
                    if (correoExiste)
                    {
                        Console.Clear();
                        Console.WriteLine("Lo sentimos, el correo que ingresó ya esta registrado");
                        CrearUsuario();
                    }
                    else
                    {
                        context.Usuarios.Add(usuario);
                        context.SaveChanges();
                        Console.WriteLine("El usuario se creó correctamente");
                        //Home();

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                Menu();
            }

        }
        /// <summary>
        /// Logearte en la aplicación
        /// </summary>
        public static void Login()
        {
            try
            {
                Console.WriteLine("------------Inicia sesión------------");
                Console.Write("Correo: ");
                string correo = Console.ReadLine();
                Console.Write("Contraseña: ");
                string password = Console.ReadLine();

                using (TienditaContext context = new TienditaContext())
                {
                    Usuario usuario = context.Usuarios.FirstOrDefault(x => x.Correo == correo);
                    if (usuario != null)
                    {
                        if (VerifyHash(new MD5CryptoServiceProvider(), password, usuario.Password))
                        {
                            Console.WriteLine("Inició sesión");
                            MenuGeneral();
                        }
                        else
                        {
                            Console.WriteLine("La contraseña ingresada, es incorrecta. Inténtelo de nuevo");
                             Login();
                        }
                    
                    }
                    else
                    {
                        Console.WriteLine("No se encontró el usuario. Inténtelo de nuevo c:");
                        Login();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                Menu();
            }
        }

        /// <summary>
        /// CRUD de Productos
        /// </summary>
        /// 
        public static void BuscarProductos()
        {
            try
            {
                Console.Write("Buscar productos: ");
                string buscar = Console.ReadLine();

                using (TienditaContext context = new TienditaContext())
                {
                    var productos = context.Productos.Where(p => p.Nombre.Contains(buscar));
                    Console.Clear();
                    Console.WriteLine("------------Lista de productos----------");
                    Console.WriteLine("______________________________________________");
                    Console.WriteLine("|  Id |     Producto     |  Costo producto   |");
                    Console.WriteLine("______________________________________________");
                    foreach (var p in productos)
                    {
                        Console.WriteLine("|" + p.Id + "|     " + p.Nombre + "     |   " + p.Costo + "MX" + "   |");
                        Console.WriteLine("______________________________________________");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuProducto();
            }
        }
      
        public static void CrearProducto()
        {
            try
            {
                Console.WriteLine("Crear producto");
                Producto producto = new Producto();
                producto = InsertarProducto(producto);

                using (TienditaContext context = new TienditaContext())
                {
                    context.Add(producto);
                    context.SaveChanges();
                    Console.WriteLine("Producto creado con exito");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuProducto();
            }
        }

        public static Producto InsertarProducto(Producto producto)
        {
            try
            {
                Console.Write("Nombre: ");
                producto.Nombre = Console.ReadLine();
                Console.Write("Descripción: ");
                producto.Descripcion = Console.ReadLine();
                Console.Write("Precio: ");
                producto.Precio = decimal.Parse(Console.ReadLine());
                Console.Write("Costo: ");
                producto.Costo = decimal.Parse(Console.ReadLine());
                Console.Write("Cantidad: ");
                producto.Cantidad = decimal.Parse(Console.ReadLine());
                Console.Write("Tamaño: ");
                producto.Tamano = Console.ReadLine();

                
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuProducto();
            }
            return producto;
        }

        public static Producto SelecionarProducto()
        {
            Console.Write("Seleciona el código de producto: ");
            int id = int.Parse(Console.ReadLine());
            using (TienditaContext context = new TienditaContext())
            {
                Producto producto = context.Productos.Find(id);
                if (producto == null)
                {
                    SelecionarProducto();
                }
                return producto;
            }
        }

        public static void ActualizarProducto()
        {
            try
            {
                Console.WriteLine("Actualizar producto");
                Producto producto = SelecionarProducto();
                producto = InsertarProducto(producto);
                using (TienditaContext context = new TienditaContext())
                {
                    context.Update(producto);
                    context.SaveChanges();
                    Console.WriteLine("Producto actualizado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuProducto();
            }
        }

        public static void EliminarProducto()
        {
            try
            {
                Console.WriteLine("Eliminar producto");
                Producto producto = SelecionarProducto();
                using (TienditaContext context = new TienditaContext())
                {
                    context.Remove(producto);
                    context.SaveChanges();
                    Console.WriteLine("Producto eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuProducto();
            }

        }

        /// <summary>
        /// CRUD de ventas
        /// </summary>

        public static void BuscarVentas()
        {
            try
            {
                Console.Write("-------Buscar ventas-------");
                int id = int.Parse(Console.ReadLine());

                using (TienditaContext context = new TienditaContext())
                {
                    var ventas = context.Ventas.Where(p => p.Id == id).ToList();
                    Console.Clear();
                    Console.WriteLine("------------Lista de ventas----------");
                    Console.WriteLine("______________________________________________");
                    Console.WriteLine("|  Id |     Cliente     |  Monto total   |");
                    Console.WriteLine("______________________________________________");

                    foreach (var v in ventas)
                    {
                        Console.WriteLine("|" + v.Id + "|     " + v.Cliente + "     |   " + v.Total + "MX" + "   |");
                        Console.WriteLine("______________________________________________");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuProducto();
            }
        }
        public static void CrearVenta()
        {
            try
            {
                Console.WriteLine("--------------Crear venta--------------");
                Venta venta = new Venta();
                venta = InsertarVenta(venta);

                using (TienditaContext context = new TienditaContext())
                {
                    context.Add(venta);
                    context.SaveChanges();
                    Console.WriteLine("Venta creada con exito");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuVenta();
            }
        }


        public static Venta InsertarVenta(Venta venta)
        {
            try
            {
                Console.Write("Total: ");
                venta.Total = decimal.Parse(Console.ReadLine());
                Console.Write("Cliente: ");
                venta.Cliente = Console.ReadLine();
                venta.Fecha = DateTime.Now;

            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuVenta();
            }
            return venta;
        }

        public static Venta SeleccionarVenta()
        {
            Console.Write("Seleciona el código de la venta: ");
            int id = int.Parse(Console.ReadLine());
            using (TienditaContext context = new TienditaContext())
            {
                Venta venta = context.Ventas.Find(id);
                if (venta == null)
                {
                    SeleccionarVenta();
                }
                return venta;
            }

        }

        public static void ActualizarVenta()
        {
            try
            {
                Console.WriteLine("Actualizar venta");
                Venta venta = SeleccionarVenta();
                venta = InsertarVenta(venta);
                using (TienditaContext context = new TienditaContext())
                {
                    context.Update(venta);
                    context.SaveChanges();
                    Console.WriteLine("Venta actualizada");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuVenta();
            }

        }

        public static void EliminarVenta()
        {
            try
            {
                Console.WriteLine("Eliminar venta");
                Venta venta = SeleccionarVenta();
                using (TienditaContext context = new TienditaContext())
                {
                    context.Remove(venta);
                    context.SaveChanges();
                    Console.WriteLine("Venta eliminada");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuVenta();
            }
        }

        /// <summary>
        /// CRUD de detalle
        /// </summary>
        public static void BuscarDetalle()
        {
            Console.WriteLine("-------------Buscar detalle----------");
            Console.Write("Ingrese el Id del detalle: ");
            int? id = int.Parse(Console.ReadLine());

            try
            {
                using (TienditaContext context = new TienditaContext())
                {
                    var detalles = context.Detalles.Include(x => x.Producto).Include(x => x.Venta).Where(x => x.Id == id).ToList();
                    if (detalles.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("---------------------Lista de detalles--------------");
                        Console.WriteLine("_________________________________________________________________________________");
                        Console.WriteLine("|  Id |     Producto     |  Costo producto   |  Total de venta   |   Subtotal   |");
                        Console.WriteLine("_________________________________________________________________________________");
                        foreach (var detalle in detalles)
                        {
                            Console.WriteLine("|" + detalle.Id + "|     " + detalle.Producto.Nombre + "     |   " + detalle.Producto.Costo + "MX" + "   |   " + detalle.Venta.Total + "MX" + "  |       " + detalle.Subtotal + "MX" + "  |");
                            Console.WriteLine("_______________________________________________________________________________");
                        }

                    }
                    else
                    {
                        Console.WriteLine("No se encontraron detalles registrados.");
                        MenuDetalle();
                    }


                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuDetalle();
            }
        }

        public static void CrearDetalle()
        {
            try
            {
                using (TienditaContext context = new TienditaContext())
                {
                    var productos = context.Productos.ToList();
                    var ventas = context.Ventas.ToList();
                    if (productos.Any() && ventas.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("------------Lista de productos----------");
                        Console.WriteLine("______________________________________________");
                        Console.WriteLine("|  Id |     Producto     |  Costo producto   |");
                        Console.WriteLine("______________________________________________");
                        foreach (var p in productos)
                        {
                            Console.WriteLine("|" + p.Id + "|     " + p.Nombre + "     |   " + p.Costo + "MX" + "   |" );
                            Console.WriteLine("______________________________________________");
                        }

                        Console.WriteLine("------------Lista de ventas----------");
                        Console.WriteLine("______________________________________________");
                        Console.WriteLine("|  Id |     Cliente     |  Monto total   |");
                        Console.WriteLine("______________________________________________");

                        foreach (var v in ventas)
                        {
                            Console.WriteLine("|" + v.Id + "|     " + v.Cliente + "     |   " + v.Total + "MX" + "   |");
                            Console.WriteLine("______________________________________________");
                        }
                    }
                    else
                    {
                        Console.Write("No se encontraron registros");
                        MenuDetalle();
                    }
                }
                Console.WriteLine("------------Crear detalle-------------");
                Detalle detalle = new Detalle();
                detalle = InsertaDetalle(detalle);

                using (TienditaContext context = new TienditaContext())
                {
                    context.Add(detalle);
                    context.SaveChanges();
                    Console.WriteLine("Detalle creado de manera éxitosa.");
                    MenuDetalle();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuDetalle();
            }
        }


        public static Detalle InsertaDetalle(Detalle detalle)
        {
            try
            {
                Producto producto = SelecionarProducto();
                detalle.ProductoId = producto.Id;
                Venta venta = SeleccionarVenta();
                detalle.VentaId = venta.Id;

                Console.Write("Subtotal: ");
                detalle.Subtotal = decimal.Parse(Console.ReadLine());


            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuDetalle();
            }
            return detalle;
        }

        public static Detalle SeleccionarDetalle()
        {
            Console.Write("Seleciona el id del detalle: ");

                int Id = int.Parse(Console.ReadLine());
                using (TienditaContext context = new TienditaContext())
                {
                    var detalle = context.Detalles.Find(Id);
                    if (detalle == null)
                    {
                        SeleccionarDetalle();
                    }
                    return detalle;
                }
        }


        public static void ActualizarDetalle()
        {
            try
            {
                Console.WriteLine("--------------Actualizar detalle------------------");
                using (TienditaContext context = new TienditaContext())
                {
                    
                    var productos = context.Productos.ToList();
                    var ventas = context.Ventas.ToList();
                    var detalles = context.Detalles.ToList();
                    if (detalles.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("---------------------Lista de detalles--------------");
                        Console.WriteLine("_________________________________________________________________________________");
                        Console.WriteLine("|  Id |     Producto     |  Costo producto   |  Total de venta   |   Subtotal   |");
                        Console.WriteLine("_________________________________________________________________________________");
                        foreach (var d in detalles)
                        {
                            Console.WriteLine("|" + d.Id + "|     " + d.Producto.Nombre + "     |   " + d.Producto.Costo + "MX" + "   |   " + d.Venta.Total + "MX" + "  |       " + d.Subtotal + "MX" + "  |");
                            Console.WriteLine("_______________________________________________________________________________");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No se encontraron detalles registrados.");
                        MenuDetalle();
                    }
                }
               
                Detalle detalle = SeleccionarDetalle();
                detalle = InsertaDetalle(detalle);
                using (TienditaContext context = new TienditaContext())
                {
                    context.Update(detalle);
                    context.SaveChanges();
                    Console.WriteLine("Detalle actualizado de manera éxitosa.");
                    MenuDetalle();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuDetalle();
            }
        }


        public static void EliminarDetalle()
        {
            Console.WriteLine("Eliminar detalle");
            try
            {
                using (TienditaContext context = new TienditaContext())
                {
                    var detalles = context.Detalles.Include(x => x.Producto).Include(x => x.Venta).ToList();
                    if (detalles.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("---------------------Lista de detalles--------------");
                        Console.WriteLine("_________________________________________________________________________________");
                        Console.WriteLine("|  Id |     Producto     |  Costo producto   |  Total de venta   |   Subtotal   |");
                        Console.WriteLine("_________________________________________________________________________________");
                        foreach (var d in detalles)
                        {
                            Console.WriteLine("|" + d.Id + "|     " + d.Producto.Nombre + "     |   " + d.Producto.Costo + "MX" + "   |   " + d.Venta.Total + "MX" + "  |       " + d.Subtotal + "MX" + "  |");
                            Console.WriteLine("_______________________________________________________________________________");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No se encontraron detalles registrados.");
                        MenuDetalle();
                    }

                    MenuDetalle();
                }
                Detalle detalle = SeleccionarDetalle();
                using (TienditaContext context = new TienditaContext())
                {
                    context.Remove(detalle);
                    context.SaveChanges();
                    Console.WriteLine("Detalle eliminado de manera éxitosa.");
                    MenuDetalle();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ups! Ocurrió un error interno :c");
                Console.WriteLine("Error:" + e.Message);

                MenuDetalle();
            }
        }

    }
}
